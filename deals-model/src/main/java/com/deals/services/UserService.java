package com.deals.services;

import com.deals.model.users.User;
import com.deals.services.base.BaseService;
import org.springframework.stereotype.Component;

@Component("userService")
public class UserService extends BaseService<User>
{
}
