package com.deals.services;

import ch.lambdaj.function.matcher.Predicate;
import com.deals.model.product.Category;
import com.deals.services.base.BaseService;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static ch.lambdaj.Lambda.filter;
import static ch.lambdaj.collection.LambdaCollections.with;

/**
 * Service that responsible for work with categories.
 */
@Component("categoryService")
public class CategoryService extends BaseService<Category>
{
    public List<Category> getAllEndCategories()
    {
        return hibernateTemplate.find("select distinct cat from Category cat where not exists(from Category child where child.parent=cat)");
    }

    public List<Category> getAllEndCategoriesWithGivenParent(final Category parent)
    {
        Predicate<Category> isGrandChildPredicate = new Predicate<Category>()
        {
            @Override
            public boolean apply(Category o)
            {
                return isGrandchild(o, parent);
            }
        };
        return filter(isGrandChildPredicate, getAllEndCategories());
    }

    private Boolean isGrandchild(Category grandChild, Category parent)
    {
        Category currentLevel = grandChild;
        if (grandChild.equals(parent))
        {
            return true;
        }
        while (currentLevel.getParent() != null)
        {
            if (currentLevel.getParent().equals(parent))
            {
                return true;
            }
            currentLevel = currentLevel.getParent();
        }
        return false;
    }

    public List<Category> getAllTopLevelCategories()
    {
        return hibernateTemplate.find("from Category cat where cat.parent is null");
    }

    /**
     * Return all parent categories that lead to current node, determined by raw string path
     *
     * @param path
     * @return
     */
    public List<Category> getCategoryPathByStringPath(String path)
    {
        if (path == null)
        {
            return new ArrayList<Category>();
        }

        String[] categories = path.split("/");
        Category lastParent = null;
        List<Category> result = new ArrayList<Category>();
        for (String category : categories)
        {
            DetachedCriteria byNameAndParent = DetachedCriteria.
                    forClass(Category.class).
                    add(Restrictions.eq("shortName", category));

            if (lastParent == null)
            {
                byNameAndParent.add(Restrictions.isNull("parent"));
            } else
            {
                byNameAndParent.add(Restrictions.eq("parent", lastParent));
            }

            List categoryByNameAndParent = hibernateTemplate.findByCriteria(byNameAndParent);
            if (categoryByNameAndParent.isEmpty())
            {
                return new ArrayList<Category>();
            } else
            {
                lastParent = (Category) categoryByNameAndParent.get(0);
                result.add(lastParent);
            }
        }
        return result;
    }

    /**
     * Get all categories that available from current node. Current node is determined by string path.
     *
     * @param path
     * @return
     */
    public List<Category> getAvailableCategories(String path)
    {
        List<Category> categoryPathByStringPath = getCategoryPathByStringPath(path);
        if (!categoryPathByStringPath.isEmpty())
        {
            Category lastCategory = categoryPathByStringPath.get(categoryPathByStringPath.size() - 1);
            return hibernateTemplate.findByCriteria(DetachedCriteria.forClass(Category.class).add(Restrictions.eq("parent", lastCategory)));
        }
        return getAllTopLevelCategories();
    }

    public Category getCurrentCategory(String path)
    {
        List<Category> categoryPathByStringPath = getCategoryPathByStringPath(path);
        if (categoryPathByStringPath.isEmpty())
        {
            return null;
        }
        return categoryPathByStringPath.get(categoryPathByStringPath.size() - 1);
    }
}
