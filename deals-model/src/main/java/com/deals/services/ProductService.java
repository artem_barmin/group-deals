package com.deals.services;

import com.deals.model.product.Category;
import com.deals.model.product.Product;
import com.deals.services.base.BaseService;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: artem
 * Date: 1/6/12
 * Time: 8:38 PM
 * To change this template use File | Settings | File Templates.
 */
@Component("productService")
public class ProductService extends BaseService<Product>
{

    @Autowired
    private CategoryService categoryService;

    public List<Product> getProductsInCategory(Category category, Integer limit, Integer offset)
    {
        List<Category> allEndCategoriesWithGivenParent = categoryService.getAllEndCategoriesWithGivenParent(category);
        DetachedCriteria productCriteria = DetachedCriteria.forClass(Product.class);
        if (!allEndCategoriesWithGivenParent.isEmpty())
            productCriteria.add(Restrictions.in("category", allEndCategoriesWithGivenParent));
        return hibernateTemplate.findByCriteria(productCriteria, offset, limit);
    }
}
