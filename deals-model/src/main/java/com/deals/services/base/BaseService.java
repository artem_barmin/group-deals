package com.deals.services.base;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Parent class for all business services. Contains generic methods for saving, updating and getting all entities.
 * <p/>
 * Also contains hibernate template as protected variable, so all services can use it without any problems.
 * <p/>
 */
public class BaseService<T>
{
    @Autowired
    public HibernateTemplate hibernateTemplate;

    @Transactional
    public Long save(T entity)
    {
        return (Long) hibernateTemplate.save(entity);
    }

    @Transactional(readOnly = true)
    public <F> List<F> loadAll(Class<F> clazz)
    {
        return hibernateTemplate.loadAll(clazz);
    }

    @Transactional(readOnly = true)
    public <F> List<F> loadAll(Class<F> clazz, Integer limit, Integer offset)
    {
        return hibernateTemplate.findByCriteria(DetachedCriteria.forClass(clazz), offset, limit);
    }

    public T get(Class<T> clazz, Long id)
    {
        return hibernateTemplate.get(clazz, id);
    }

}
