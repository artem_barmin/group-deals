package com.deals.generator;

import com.deals.model.product.Category;
import com.deals.model.product.GroupDeal;
import com.deals.model.product.ProductInfo;
import com.deals.model.product.Stock;
import com.deals.model.support.Image;
import com.deals.model.users.User;
import com.deals.model.users.roles.Organizer;
import com.deals.services.CategoryService;
import com.deals.services.ProductService;
import com.deals.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Generator of products(specifically - group deals)
 */
@Component("productGenerator")
@Slf4j
public class ProductGenerator
{
    @Autowired
    private ProductService productService;

    @Autowired
    private UserGenerator userGenerator;

    @Autowired
    private CategoryGenerator categoryGenerator;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private UserService userService;

    public void generateDeals(int count)
    {
        userGenerator.generateUsers(100);
        List<User> users = userService.loadAll(User.class);

        categoryGenerator.generateCategories();
        List<Category> categories = categoryService.getAllEndCategories();

        try
        {
            for (int i = 0; i < count; i++)
            {
                GroupDeal product = new GroupDeal();

                product.setName("GD " + i);
                product.setState(GroupDeal.GroupDealState.REQUESTS_COLLECTION);

                product.setOrganizer((Organizer) users.get(0).getRoles().get(1));

                product.addNewInfoVersion(createProductInfo(i));

                product.setCategory(categories.get( i % categories.size()));

                productService.save(product);
            }
        } catch (Exception ex)
        {
            log.error("Problems while creating products", ex);
        }
    }

    private ProductInfo createProductInfo(int number)
    {
        int i = number % 5 + 1;

        ProductInfo productInfo = new ProductInfo();
        productInfo.setShowcase(new ArrayList<Image>());
        productInfo.setDescription("Покупать образование нью на, бог на ничего работали. Те необходимо начальники возможностей чем, нью те пять коду прийти. Там то хороша двигает, бог сразу вернёмся во, оно покупать электронной он. Тем до труда процессорах. То мои количества собственный, по существует распоряжения лет, ну свой весьма двигает тд.\n" +
                "\n" +
                "Мог за имеет вопрос программного. Почему отключаются нее от, за них починить размером, программировать низкокачественный чем до. Там разным мешают сомнений на. Чем не йорке программы превосходные, ты вот обычно написано.\n" +
                "\n" +
                "Различных эзотерическая по не. Этих консалтинговые до нас, фон хочу возникновения те, вот об пора будет главной. Ещё но просто запускали концентрации, был многие йельский существует бы. Нью об шагать решить, но одно байтах постигнет мог. Вещь кусочек не оно, нет до вокруг полазить программировать.\n" +
                "\n" +
                "Те пусть медицинское опа, код до внешних редактор, гора самом об был. Для стиле джоель творческую во, тд автора страниц удовольствием бы. Ну фирме дизайну был, что их свою важно лучшему. Хочу работу дизайну их ещё, до хочу этого код. Вы даже после две, почту делаете остальные миф на. Даже будет опа их, ну многие кремния продлить все.\n" +
                "\n" +
                "Об его деле выпуска поработать, кто бы могут большим. Опа ты шесть отдавали, оно дурак конечного не. Фирме хороший вы над. Могу многие превосходные лет мы. Пора лагерю означает до тем, нанять добиться появления но всё, опа быть начать главной мы.\n" +
                "\n" +
                "Нее об интервью содержимое. Опа не оставляя программном. Назад предлагаю пожалуйста ну над, вопрос русском нет он. Несколько отказаться ещё ну, ты лет силы стимул. Он опа сети работали. Адрес может то тд, там колёса основам правильно ты.\n" +
                "\n" +
                "По этот ушёл огонь не, многие кэмбридже без мы. Минуты работал выпускать не них. Отнимет заложенным его во, над ведь почты погодите на, вы сразу вреде эти. Другие компанию за дни. Тд полностью поскольку впечатляющих об.\n" +
                "\n" +
                "Были черт предварительного во миф. Об про большим компании. Мешают отложим пользователи всё те, джоель одиночку те чем. Те не царь программного возникновения. Нет кажется программе ну. Во комнату оркестра уже, там их быть человек обеспечения, бы коду ошибки система или.\n" +
                "\n" +
                "Нейманом определить ты нью, силы творческую отключаются как бы, вот дурак другие осколки вы. За усилий уровней образование уже, те них люди дизайну, творческую несерьёзный он нью. Вас джоель чёртов результаты но, до потому остальных электронной все. По программ подумаем остальных фон, но опа учёт знаем работаешь. Маки какие другие вы оно, для по вещь разным, это могут вокруг ты. Не стиле образом стряпающийся нет.\n" +
                "\n" +
                "Вообще полностью до над, бог но система стратегические. Можно шагать руководишь бы код. Те над больше работу успехов. Уровня касается то не, об бог какие сомнений состоянии, бог бы кровью ничего работникам. То улице байтах мои, где за кучу состояние, ведь собой мы бог.\n" +
                "      ");

        Image image = new Image();
        image.setPath("/resources/images/" + i + ".jpg");
        image.setThumbnail("/resources/images/thumbs/" + i + ".jpg");
        image.setDescription("Short description for image " + i);

        productInfo.getShowcase().add(image);

        productInfo.setStocks(new ArrayList<Stock>());

        Stock stock = new Stock();
        stock.setName("stock1");
        stock.setImage(image);
        stock.setDescription("description");

        productInfo.getStocks().add(stock);

        return productInfo;
    }

}
