package com.deals.generator;

import com.deals.model.users.User;
import com.deals.model.users.UserInfo;
import com.deals.model.users.roles.Administrator;
import com.deals.model.users.roles.Organizer;
import com.deals.model.users.roles.Role;
import com.deals.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * Really simple generator of users
 */
@Component("userGenerator")
@Slf4j
public class UserGenerator
{

    @Autowired
    private UserService userService;

    public void generateUsers(int count)
    {
        try
        {
            for (int i = 0; i < count; i++)
            {
                User user = new User();
                UserInfo userInfo = new UserInfo();

                userInfo.setName("artem" + i);
                userInfo.setEmail("a@a.com");
                userInfo.setPassword("password" + i);

                user.setIsBanned(false);

                user.setUserInfo(userInfo);
                user.setRoles(new ArrayList<Role>());
                user.getRoles().add(new Administrator());
                user.getRoles().add(new Organizer());
                userService.save(user);
            }
        } catch (Exception ex)
        {
            log.error("Problems while creating users", ex);
        }
    }
}
