package com.deals.generator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.PostConstruct;

/**
 * Bean to overcome Spring 'init' problem with transactions.
 *
 * IMPORTANT : All generators must be injected here and executed using transactionTemplate. This done because
 * Spring load time weaving(that used for processing @Transactional annotation) is working bad together with
 * Jrebel. You must do this only for generators(I think this is because JMX interfering with transaction demarcation
 * mechanism).
 */
@Component("initialGenerator")
@ManagedResource(objectName = "generator:bean=mainGenerator", description = "Entry point for all generators")
public class InitialGenerator
{
    @Autowired
    private ProductGenerator productGenerator;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @ManagedOperation
    @PostConstruct
    public void init()
    {
        transactionTemplate.execute(new TransactionCallback<Object>()
        {
            public Object doInTransaction(TransactionStatus status)
            {
                productGenerator.generateDeals(500);
                return null;
            }
        });
    }

}
