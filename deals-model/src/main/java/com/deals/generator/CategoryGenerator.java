package com.deals.generator;

import com.deals.model.product.Category;
import com.deals.services.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * Generate bunch of standart categories for testing
 */
@Component("categoryGenerator")
@Slf4j
public class CategoryGenerator
{

    @Autowired
    CategoryService categoryService;

    public void generateCategories()
    {
        Category dress = new Category("Одежда для взрослых", "adults", null);
        Category woman = new Category("Женская одежда", "female", dress);
        Category street = new Category("Верхняя одежда", "overcoat", woman);

        List<Category> categories = Arrays.asList(new Category("Пальто", "coats", street),
                new Category("Куртки", "jackets", street),
                new Category("Плащи", "raincoats", street),
                new Category("Ветровки", "windcoats", street),
                new Category("Жакеты", "jackets", street));

        categoryService.save(dress);
        categoryService.save(woman);
        categoryService.save(street);
        for (Category category : categories)
            categoryService.save(category);
    }
}
