package com.deals.model.product;

import com.deals.model.AbstractEntity;
import com.deals.model.support.Image;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

/**
 * Entity that represents one stock item.
 */
@Entity
@Data
public class Stock extends AbstractEntity
{
    @NotEmpty
    String name;

    @NotEmpty
    String description;

    @NotNull @OneToOne
    Image image;
}

