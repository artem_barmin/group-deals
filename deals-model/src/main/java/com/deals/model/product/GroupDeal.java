package com.deals.model.product;

import com.deals.model.messages.MessageBox;
import com.deals.support.Initialize;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

/**
 * AbstractEntity that represent group deal.
 * <p/>
 * What it introduce compared to Product:
 * <ol>
 * <li>complex life cycle management</li>
 * </ol>
 */
@Entity
public class GroupDeal extends Product
{
    public enum GroupDealState
    {
        REQUESTS_COLLECTION,
        ORDER_PROCESSING,
        MONEY_COLLECTION,
        WAITING_FOR_STOCK,
        DISTRIBUTION,
        ARCHIVE
    }

    @NotNull @Getter @Setter
    GroupDealState state;

    @OneToOne(cascade = CascadeType.ALL) @Initialize @Getter
    MessageBox messageBox;
}
