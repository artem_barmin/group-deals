package com.deals.model.product;

import com.deals.model.AbstractEntity;
import lombok.Data;

import javax.persistence.Entity;

/**
 * Value-object that represent various conditions, that can be assigned to product. It should be used only for
 * informative reasons.
 * <p/>
 * Examples:
 * <ol>
 * <li>Amount of subscribers, needed for closing group deal must be greater then 10</li>
 * <li>This product will be active until 10 January 2012 year</li>
 * </ol>
 */
@Entity
@Data
public class Condition extends AbstractEntity
{
    public enum Type
    {
        ALL_SIZES,
        MONEY,
        TIME
    }
}
