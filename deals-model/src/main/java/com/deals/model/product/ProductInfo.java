package com.deals.model.product;

import com.deals.model.AbstractEntity;
import com.deals.model.support.Image;
import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Set;

/**
 * Value object that contains all information about concrete version of product info.
 */
@Entity
@Data
public class ProductInfo extends AbstractEntity
{
    @OneToMany
    Set<Condition> conditions;

    @OneToMany(cascade = CascadeType.ALL) @OrderColumn(name = "stockPosition")
    List<Stock> stocks;

    @OneToMany(cascade = CascadeType.ALL) @NotNull @Size(min = 1) @OrderColumn(name = "showcaseNumber")
    List<Image> showcase;

    @Size(max = 5000)
    String description;
}
