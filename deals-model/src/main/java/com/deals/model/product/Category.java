package com.deals.model.product;

import com.deals.model.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * Value object that represent some category for group deal. Categories can also form hierarchy.
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"parent", "name"}, callSuper = false)
public class Category extends AbstractEntity
{
    @NotNull
    String name;

    @NotNull
    String shortName;

    @ManyToOne
    Category parent;
}
