package com.deals.model.product;

import com.deals.model.AbstractEntity;
import com.deals.model.users.roles.Organizer;
import com.deals.model.users.roles.Subscriber;
import com.deals.support.Initialize;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Parent of all products. Contains info about single product, also responsible for maintaining versions of product info.
 * <p/>
 * Contains following information:
 * <ol>
 * <li>set of all active subscribers</li>
 * <li>organizer of current product</li>
 * <li>current active version of information</li>
 * <li>ordered list of all previous versions of information, position in list = version number, current active
 * version also presented in list as last item</li>
 * </ol>
 */
@Entity
public abstract class Product extends AbstractEntity
{
    @Getter @Setter @Size(max = 50)
    String name;

    @OneToMany @Valid @Getter
    Set<Subscriber> subscribers;

    @OneToMany @Getter @Setter
    Set<Stock> stocks;

    @ManyToOne @NotNull @Valid @Getter @Setter
    Organizer organizer;

    @OneToOne(cascade = CascadeType.ALL) @NotNull @Valid @Initialize
    ProductInfo info;

    @OneToMany @Valid @OrderColumn(name = "infoVersion") @Size(min = 1) @Initialize
    List<ProductInfo> infoVersions;

    @ManyToOne @Getter @Setter @NotNull
    Category category;

    public void addNewInfoVersion(ProductInfo info)
    {
        this.info = info;
        infoVersions.add(info);
    }

    public ProductInfo getActiveVersion()
    {
        return info;
    }

    public void addSubscriber(Subscriber user)
    {
        if (subscribers == null)
        {
            subscribers = new HashSet<Subscriber>();
        }
        subscribers.add(user);
    }

    public void removeSubscriber(Subscriber user)
    {
        // TODO : some complex logic that decrease reputation of user, and other stuff
        subscribers.remove(user);
    }

}
