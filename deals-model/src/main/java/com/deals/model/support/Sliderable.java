package com.deals.model.support;

/**
 * Marker interface for entities that can be used as a model for the image slider.
 * These entities must have the fields imagePath (to contain the path to the image),
 * thumbnailsPath (to contain the path to a thumbnail)
 * and imageDescription (text accompaniment to the main image).
 */
public interface Sliderable {
}
