package com.deals.model.support;

import com.deals.model.AbstractEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

/**
 * Special class for storing information about image(can also have thumbnail and description, specific for the image)
 */
@Entity
@Data
public class Image extends AbstractEntity implements Sliderable
{
    @NotNull
    String path;

    String thumbnail;

    String description;
}
