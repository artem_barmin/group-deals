package com.deals.model.users.roles;

import javax.persistence.Entity;

/**
 * Role for Subscriber - the man who take part in the group deal. He want to buy some cheap and nifty items.
 */
@Entity
public class Subscriber extends Role
{
}
