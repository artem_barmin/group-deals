package com.deals.model.users.roles;

import javax.persistence.Entity;

/**
 * Man that responsible for general site management. He have all rights.
 */
@Entity
public class Administrator extends Role
{
}
