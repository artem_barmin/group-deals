package com.deals.model.users;

import lombok.Data;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Embeddable;

@Embeddable
@Data
public class UserInfo
{
    @NotEmpty
    String name;

    @Email @NotEmpty
    String email;

    @NotEmpty
    String password;
}