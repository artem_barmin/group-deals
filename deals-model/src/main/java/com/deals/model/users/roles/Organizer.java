package com.deals.model.users.roles;

import javax.persistence.Entity;

/**
 * Role for Organizer - the man who creates group deals, and responsible for changing theirs status, and other management.
 */
@Entity
public class Organizer extends Role
{
}
