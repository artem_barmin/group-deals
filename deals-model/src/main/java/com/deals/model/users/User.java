package com.deals.model.users;

import com.deals.model.AbstractEntity;
import com.deals.support.Initialize;
import com.deals.model.users.roles.Role;
import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Data
public class User extends AbstractEntity
{
    @Valid @NotNull @Initialize
    UserInfo userInfo;

    @NotNull
    Boolean isBanned;

    @OneToMany(cascade = CascadeType.ALL) @OrderColumn(name = "roleIndex") @Initialize
    List<Role> roles;
}
