package com.deals.model.users.roles;

import com.deals.model.AbstractEntity;
import com.deals.support.Initialize;
import com.deals.model.messages.MessageBox;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Parent of all user roles. Responsibility : make possible store set of roles for single user.
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Data
public abstract class Role extends AbstractEntity
{
    @OneToOne(cascade = CascadeType.ALL) @NotNull @Initialize
    MessageBox messageBox;
}
