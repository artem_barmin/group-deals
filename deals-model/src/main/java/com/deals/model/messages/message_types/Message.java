package com.deals.model.messages.message_types;

import com.deals.model.AbstractEntity;
import com.deals.model.users.roles.Role;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Parent of all message types.
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Data
public abstract class Message extends AbstractEntity
{
    @OneToOne @NotNull
    Role sender;

    @NotNull
    String text;

    @Temporal(TemporalType.TIMESTAMP) @NotNull
    Date time;
}
