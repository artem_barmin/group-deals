package com.deals.model.messages.message_types;

import javax.persistence.Entity;

/**
 * Entity for message from public discussion.
 */
@Entity
public class DiscussionMessage extends Message
{
}
