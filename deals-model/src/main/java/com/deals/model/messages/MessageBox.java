package com.deals.model.messages;

import com.deals.model.AbstractEntity;
import com.deals.model.RepositorySupport;
import com.deals.model.messages.message_types.Message;
import com.deals.support.Initialize;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.List;

/**
 * Entity for storing all sent and received messages.
 */
@Entity
public class MessageBox extends AbstractEntity
{
    @Initialize @Transient RepositorySupport repositorySupport;

    @ManyToMany(cascade = CascadeType.ALL) @Initialize
    @JoinTable(name = "inbox_messages", joinColumns = {@JoinColumn(name = "messagebox_id")}, inverseJoinColumns = {@JoinColumn(name = "message_id")})
    List<Message> inbox;

    @ManyToMany(cascade = CascadeType.ALL) @Initialize
    @JoinTable(name = "sent_messages", joinColumns = {@JoinColumn(name = "messagebox_id")}, inverseJoinColumns = {@JoinColumn(name = "message_id")})
    List<Message> sent;

    @Transactional
    public void send(MessageBox target, Message message)
    {
        target.inbox.add(message);
        target.sent.add(message);
    }

    public List<Message> getAllDiscussionMessages()
    {
        return repositorySupport.template.find("select message from MessageBox mb " +
                "join mb.inbox message " +
                "where message.class=DiscussionMessage and mb.id=?", this.getId());
    }
}
