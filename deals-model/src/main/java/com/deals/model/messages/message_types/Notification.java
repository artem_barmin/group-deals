package com.deals.model.messages.message_types;

import lombok.Data;

import javax.persistence.Entity;

/**
 * Entity that represent some user notification.
 * <p/>
 * Examples:
 * <ol>
 * <li>deal condition was changed(notification for subscriber)</li>
 * <li>new user was subscribed to deal(notification for organizer)</li>
 * </ol>
 */
@Entity
@Data
public class Notification extends Message
{
    public enum NotificationStatus
    {
        READ,
        UNREAD
    }

    NotificationStatus status;
}
