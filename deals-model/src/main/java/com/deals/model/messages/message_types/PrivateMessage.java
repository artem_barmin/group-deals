package com.deals.model.messages.message_types;

import lombok.Data;

import javax.persistence.Entity;

/**
 * Entity for private messages between users.
 * <p/>
 * Specific part: can have special status
 */
@Entity
@Data
public class PrivateMessage extends Message
{
    public enum MessageStatus
    {
        READ,
        UNREAD,
        DELETED
    }

    MessageStatus status;
}
