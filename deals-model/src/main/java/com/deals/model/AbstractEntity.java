package com.deals.model;

import com.deals.support.InitializeSupport;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import javax.persistence.*;

/**
 * Parent of all entities. Main responsibility : contains id field.
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@ToString
@EqualsAndHashCode
public class AbstractEntity
{
    @Id @Getter @GeneratedValue(strategy = GenerationType.SEQUENCE)
    Long id;

    public AbstractEntity()
    {
        InitializeSupport.initializeFromAnnotations(getClass(), this);
    }
}
