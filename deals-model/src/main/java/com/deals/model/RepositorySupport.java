package com.deals.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.orm.hibernate3.HibernateTemplate;

/**
 * Should be added to entity in case when some of business logic should be implemented directly in domain object.
 * This class allows access to hibernate template.
 */
@Configurable
public class RepositorySupport
{
    @Autowired
    public HibernateTemplate template;
}
