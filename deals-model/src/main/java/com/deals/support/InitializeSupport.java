package com.deals.support;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 */
public class InitializeSupport
{
    public static void initializeFromAnnotations(Class clazz, Object instance)
    {
        List<Field> fields = new ArrayList<Field>(Arrays.asList(clazz.getDeclaredFields()));

        clazz = clazz.getSuperclass();
        while (clazz != null)
        {
            fields.addAll(Arrays.asList(clazz.getDeclaredFields()));
            clazz = clazz.getSuperclass();
        }

        for (Field field : fields)
        {
            if (field.getAnnotation(Initialize.class) != null)
            {
                try
                {
                    Boolean access = field.isAccessible();
                    field.setAccessible(true);
                    if (field.getType().equals(List.class))
                    {
                        field.set(instance, new ArrayList());
                    } else
                    {
                        field.set(instance, field.getType().newInstance());
                    }
                    field.setAccessible(access);
                } catch (Exception e)
                {
                    throw new RuntimeException("Runtime problem while processing @Initialize annotations", e);
                }
            }
        }
    }
}
