package com.deals.support;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Annotation for field that should be initialized automatically on new instance creation.
 */
@Retention(RUNTIME)
public @interface Initialize
{
}
