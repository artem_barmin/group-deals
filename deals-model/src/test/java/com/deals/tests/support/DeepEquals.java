package com.deals.tests.support;

import org.springframework.util.StringUtils;
import org.testng.Assert;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Test two objects for equivalence with a 'deep' comparison.  This will traverse
 * the Object graph and perform either a field-by-field comparison on each
 * object (if no .equals() method has been overridden from Object), or it
 * will call the customized .equals() method if it exists.  This method will
 * allow object graphs loaded at different times (with different object ids)
 * to be reliably compared.  Object.equals() / Object.hashCode() rely on the
 * object's identity, which would not consider two equivalent objects necessarily
 * equals.  This allows graphs containing instances of Classes that did not
 * overide .equals() / .hashCode() to be compared.  For example, testing for
 * existence in a cache.  Relying on an object's identity will not locate an
 * equivalent object in a cache.<br/><br/>
 * <p/>
 * This method will handle cycles correctly, for example A->B->C->A.  Suppose a and
 * a' are two separate instances of A with the same values for all fields on
 * A, B, and C.  Then a.assertEquals(a') will return true.  It uses cycle detection
 * storing visited objects in a Set to prevent endless loops.
 *
 * @author John DeRegnaucourt (jdereg@gmail.com)
 *         <br/>
 *         Copyright [2010] John DeRegnaucourt
 *         <br/><br/>
 *         Licensed under the Apache License, Version 2.0 (the "License");
 *         you may not use this file except in compliance with the License.
 *         You may obtain a copy of the License at
 *         <br/><br/>
 *         http://www.apache.org/licenses/LICENSE-2.0
 *         <br/><br/>
 *         Unless required by applicable law or agreed to in writing, software
 *         distributed under the License is distributed on an "AS IS" BASIS,
 *         WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *         See the License for the specific language governing permissions and
 *         limitations under the License.
 */
public class DeepEquals
{
    private static final Map<Class, Boolean> _customEquals = new ConcurrentHashMap<Class, Boolean>();
    private static final Map<Class, Boolean> _customHash = new ConcurrentHashMap<Class, Boolean>();
    private static final Map<Class, Collection<Field>> _reflectedFields = new ConcurrentHashMap<Class, Collection<Field>>();

    private static class DataStackTrace
    {
        private List<String> stackTrace;

        private DataStackTrace(List<String> stackTrace)
        {
            this.stackTrace = stackTrace;
        }

        public DataStackTrace add(String position)
        {
            ArrayList<String> list = new ArrayList<String>();
            list.addAll(stackTrace);
            list.add(position);
            return new DataStackTrace(list);
        }


        @Override
        public String toString()
        {
            return "DataStackTrace{" + "stackTrace=" + StringUtils.collectionToDelimitedString(stackTrace, ".") + '}';
        }
    }

    private static class Failure
    {
        private String message;
        private DataStackTrace stackTrace;

        private Failure(String message, DataStackTrace stackTrace)
        {
            this.message = message;
            this.stackTrace = stackTrace;
        }


        @Override
        public String toString()
        {
            return "Failure{" + "message='" + message + '\'' + "\n\t,stackTrace=" + stackTrace + '}';
        }
    }

    public static DataStackTrace stackTraceSingleton(String position)
    {
        ArrayList<String> list = new ArrayList<String>();
        list.add(position);
        return new DataStackTrace(list);
    }


    private static class DualKey
    {
        private final Object _key1;
        private final Object _key2;
        private final DataStackTrace stackPosition;

        private DualKey(Object k1, Object k2, DataStackTrace stackPosition)
        {
            _key1 = k1;
            _key2 = k2;
            this.stackPosition = stackPosition;
        }

        public boolean equals(Object other)
        {
            if (other == null)
            {
                return false;
            }

            if (!(other instanceof DualKey))
            {
                return false;
            }

            DualKey that = (DualKey) other;
            return _key1 == that._key1 && _key2 == that._key2;
        }

        public int hashCode()
        {
            int h1 = _key1 != null ? _key1.hashCode() : 0;
            int h2 = _key2 != null ? _key2.hashCode() : 0;
            return h1 + h2;
        }
    }

    /**
     * Compare two objects with a 'deep' comparison.  This will traverse the
     * Object graph and perform either a field-by-field comparison on each
     * object (if no .equals() method has been overridden from Object), or it
     * will call the customized .equals() method if it exists.  This method will
     * allow object graphs loaded at different times (with different object ids)
     * to be reliably compared.  Object.equals() / Object.hashCode() rely on the
     * object's identity, which would not consider to equivalent objects necessarily
     * equals.  This allows graphs containing instances of Classes that did no
     * overide .equals() / .hashCode() to be compared.  For example, testing for
     * existence in a cache.  Relying on an objects identity will not locate an
     * object in cache, yet relying on it being equivalent will.<br/><br/>
     * <p/>
     * This method will handle cycles correctly, for example A->B->C->A.  Suppose a and
     * a' are two separate instances of the A with the same values for all fields on
     * A, B, and C.  Then a.assertEquals(a') will return true.  It uses cycle detection
     * storing visited objects in a Set to prevent endless loops.
     *
     * @param a             Object one to compare
     * @param b             Object two to compare
     * @param namesToIgnore names of fields that should be ignored
     * @param inverseIgnore
     */
    public static void assertEquals(final Object a, Object b, final List<String> namesToIgnore, String message, Boolean inverseIgnore)
    {
        final Set<Failure> failures = new HashSet<Failure>();
        final Set<DualKey> visited = new HashSet<DualKey>();
        final LinkedList<DualKey> stack = new LinkedList<DualKey>();
        stack.addFirst(new DualKey(a, b, stackTraceSingleton(a.getClass().getSimpleName())));

        while (!stack.isEmpty())
        {
            DualKey dualKey = stack.removeFirst();
            visited.add(dualKey);

            if (dualKey._key1 == dualKey._key2)
            {   // Same instance is always equal to itself.
                continue;
            }

            // Handle Hibernate specific transformations - if some field have 'null' value, after persistension it can
            // transform all ArrayLists, HashSets from null to empty object
            if (dualKey._key1 == null ^ dualKey._key2 == null)
            {
                if (dualKey._key1 == null)
                {
                    if (dualKey._key2 instanceof Collection && ((Collection) dualKey._key2).size() == 0) continue;
                    else
                    {
                        failWithStack(failures, "Collection of the second dto does not corresponds to collection of the first dto(with respect of persistence)", dualKey.stackPosition);
                        continue;
                    }
                }
                if (dualKey._key2 == null)
                {
                    if (dualKey._key1 instanceof Collection && ((Collection) dualKey._key1).size() == 0) continue;
                    else
                    {
                        failWithStack(failures, "Collection of the first dto does not corresponds to collection of the second dto(with respect of persistence)", dualKey.stackPosition);
                        continue;
                    }
                }
            }

            if (dualKey._key1 == null || dualKey._key2 == null)
            {   // If either one is null, not equal (both can't be null, due to above comparison).
                failWithStack(failures, "One of the fields is null", dualKey.stackPosition);
                continue;
            }

            if (!dualKey._key1.getClass().equals(dualKey._key2.getClass()))
            {
                // to make checking more simple we will process special case of this rule : Arrays.asList
                // returns Arrays$ArrayList class, and if we meet it - it should be regarded equals
                if (!((dualKey._key1.getClass().equals(Arrays.asList().getClass()) && dualKey._key2.getClass().equals(ArrayList.class)) || (dualKey._key2.getClass().equals(Arrays.asList().getClass()) && dualKey._key1.getClass().equals(ArrayList.class))))
                {
                    // Must be same class
                    failWithStack(failures, "Fields have different classes : '" + dualKey._key1.getClass() + "' and '" + dualKey._key2.getClass(), dualKey.stackPosition);
                    continue;
                }
            }

            // Handle all [] types.  In order to be equal, the arrays must be the same 
            // length, be of the same type, be in the same order, and all elements within
            // the array must be deeply equivalent.
            if (dualKey._key1.getClass().isArray())
            {
                compareArrays(dualKey._key1, dualKey._key2, stack, visited, dualKey.stackPosition, failures);
                continue;
            }

            // Special handle SortedSets because they are fast to compare because their
            // elements must be in the same order to be equivalent Sets.
            if (dualKey._key1 instanceof SortedSet)
            {
                compareOrderedCollection((Collection) dualKey._key1, (Collection) dualKey._key2, stack, visited, dualKey.stackPosition, failures);
                continue;
            }

            // Handled unordered Sets.  This is a slightly more expensive comparison because order cannot
            // be assumed, a temporary Map must be created, however the comparison still runs in O(N) time.
            if (dualKey._key1 instanceof Set)
            {
                compareUnorderedCollection((Collection) dualKey._key1, (Collection) dualKey._key2, stack, visited, namesToIgnore, dualKey.stackPosition, failures, inverseIgnore);
                continue;
            }

            // Check any Collection that is not a Set.  In these cases, element order
            // matters, therefore this comparison is faster than using unordered comparison.
            if (dualKey._key1 instanceof Collection)
            {
                compareOrderedCollection((Collection) dualKey._key1, (Collection) dualKey._key2, stack, visited, dualKey.stackPosition, failures);
                continue;
            }

            // Compare two SortedMaps.  This takes advantage of the fact that these
            // Maps can be compared in O(N) time due to their ordering.
            if (dualKey._key1 instanceof SortedMap)
            {
                compareSortedMap((SortedMap) dualKey._key1, (SortedMap) dualKey._key2, stack, visited, dualKey.stackPosition, failures);
                continue;
            }

            // Compare two Unordered Maps. This is a slightly more expensive comparison because
            // order cannot be assumed, therefore a temporary Map must be created, however the
            // comparison still runs in O(N) time.
            if (dualKey._key1 instanceof Map)
            {
                compareUnorderedMap((Map) dualKey._key1, (Map) dualKey._key2, stack, visited, namesToIgnore, dualKey.stackPosition, failures, inverseIgnore);
                continue;
            }

            if (hasCustomEquals(dualKey._key1.getClass()))
            {
                if (!dualKey._key1.equals(dualKey._key2))
                {
                    failWithStack(failures, "Custom equal implementation gives negative result", dualKey.stackPosition.add("equals(" + dualKey._key1 + "," + dualKey._key2 + ")"));
                }
                continue;
            }

            Collection<Field> fields = getDeepDeclaredFields(dualKey._key1.getClass(), namesToIgnore, inverseIgnore);

            for (Field field : fields)
            {
                try
                {
                    DualKey dk = new DualKey(field.get(dualKey._key1), field.get(dualKey._key2), dualKey.stackPosition.add(field.getName()));
                    if (!visited.contains(dk))
                    {
                        stack.addFirst(dk);
                    }
                } catch (Exception ignored)
                {
                }
            }
        }

        if (failures.size() != 0)
            Assert.fail(message + "\n" + StringUtils.collectionToDelimitedString(failures, "\n", "{", "}"));
    }

    public static void assertEquals(final Object a, Object b, final List<String> namesToIgnore, String message)
    {
        assertEquals(a, b, namesToIgnore, message, false);
    }

    public static void assertEquals(final Object a, Object b, final List<String> namesToIgnore)
    {
        assertEquals(a, b, namesToIgnore, "", false);
    }

    public static void assertEquals(final Object a, Object b, final String message)
    {
        assertEquals(a, b, Collections.<String>emptyList(), message, false);
    }


    /**
     * Deeply compare to Arrays []. Both arrays must be of the same type, same length, and all
     * elements within the arrays must be deeply equal in order to return true.
     *
     * @param array1        [] type (Object[], String[], etc.)
     * @param array2        [] type (Object[], String[], etc.)
     * @param stack         add items to compare to the Stack (Stack versus recursion)
     * @param visited       Set of objects already compared (prevents cycles)
     * @param stackPosition
     * @param failures
     */
    @SuppressWarnings("unchecked")
    private static void compareArrays(Object array1, Object array2, LinkedList stack, Set visited, DataStackTrace stackPosition, Set<Failure> failures)
    {
        // Same instance check already performed...

        int len = Array.getLength(array1);
        if (len != Array.getLength(array2))
        {
            failWithStack(failures, "Array sizes are not equal : ", stackPosition);
            return;
        }

        for (int i = 0; i < len; i++)
        {
            DualKey dk = new DualKey(Array.get(array1, i), Array.get(array2, i), stackPosition.add("get(" + String.valueOf(i) + ")"));
            if (!visited.contains(dk))
            {   // push contents for further comparison
                stack.addFirst(dk);
            }
        }
    }

    /**
     * Deeply compare two Collections that must be same length and in same order.
     *
     * @param col1          First collection of items to compare
     * @param col2          Second collection of items to compare
     * @param stack         add items to compare to the Stack (Stack versus recursion)
     * @param visited       Set of objects already compared (prevents cycles)
     *                      value of 'true' indicates that the Collections may be equal, and the sets
     * @param stackPosition
     * @param failures
     * @return false if size is not equals, otherwise - true
     */
    @SuppressWarnings("unchecked")
    private static void compareOrderedCollection(Collection col1, Collection col2, LinkedList stack, Set visited, DataStackTrace stackPosition, Set<Failure> failures)
    {
        // Same instance check already performed...

        if (col1.size() != col2.size())
        {
            failWithStack(failures, "Collection sizes are not equal", stackPosition);
            return;
        }

        Iterator i1 = col1.iterator();
        Iterator i2 = col2.iterator();
        Long currentPositionInCollection = 0l;

        while (i1.hasNext())
        {
            DualKey dk = new DualKey(i1.next(), i2.next(), stackPosition.add("[" + String.valueOf(currentPositionInCollection++) + "]"));
            if (!visited.contains(dk))
            {   // push contents for further comparison
                stack.addFirst(dk);
            }
        }
    }

    /**
     * Deeply compare the two sets referenced by dualKey.  This method attempts
     * to quickly determine inequality by length, then if lengths match, it
     * places one collection into a temporary Map by deepHashCode(), so that it
     * can walk the other collection and look for each item in the map, which
     * runs in O(N) time, rather than an O(N^2) lookup that would occur if each
     * item from collection one was scanned for in collection two.
     *
     * @param col1          First collection of items to compare
     * @param col2          Second collection of items to compare
     * @param stack         add items to compare to the Stack (Stack versus recursion)
     * @param visited       Set containing items that have already been compared,
     *                      so as to prevent cycles.
     * @param namesToIgnore
     * @param stackPosition
     * @param failures
     * @param inverseIgnore
     * @return boolean false if the Collections are for certain not equals. A
     *         value of 'true' indicates that the Collections may be equal, and the sets
     *         items will be added to the Stack for further comparison.
     */
    @SuppressWarnings("unchecked")
    private static void compareUnorderedCollection(Collection col1, Collection col2, LinkedList stack, Set visited, List<String> namesToIgnore, DataStackTrace stackPosition, Set<Failure> failures, Boolean inverseIgnore)
    {
        // Same instance check already performed...

        if (col1.size() != col2.size())
        {
            failWithStack(failures, "Unordered collection sizes are not equals", stackPosition);
            return;
        }

        Map fastLookup = new HashMap();
        for (Object o : col2)
        {
            fastLookup.put(deepHashCode(o, namesToIgnore, inverseIgnore), o);
        }

        for (Object o : col1)
        {
            Object other = fastLookup.get(deepHashCode(o, namesToIgnore, inverseIgnore));
            if (other == null)
            {   // Item not even found in other Collection, no need to continue.
                failWithStack(failures, "Item not found in the collection", stackPosition.add("get(" + o.toString() + ")"));
                return;
            }

            DualKey dk = new DualKey(o, other, stackPosition);
            if (!visited.contains(dk))
            {   // Place items on 'stack' for further comparison.
                stack.addFirst(dk);
            }
        }
    }

    /**
     * Deeply compare two SortedMap instances.  This method walks the Maps in order,
     * taking advantage of the fact that they Maps are SortedMaps.
     *
     * @param map1          SortedMap one
     * @param map2          SortedMap two
     * @param stack         add items to compare to the Stack (Stack versus recursion)
     * @param visited       Set containing items that have already been compared, to prevent cycles.
     * @param stackPosition
     * @param failures
     * @return false if the Maps are for certain not equals.  'true' indicates that 'on the surface' the maps
     *         are equal, however, it will place the contents of the Maps on the stack for further comparisons.
     */
    @SuppressWarnings("unchecked")
    private static void compareSortedMap(SortedMap map1, SortedMap map2, LinkedList stack, Set visited, DataStackTrace stackPosition, Set<Failure> failures)
    {
        // Same instance check already performed...

        if (map1.size() != map2.size())
        {
            failWithStack(failures, "Sorted maps size are not equals", stackPosition);
            return;
        }

        Iterator i1 = map1.entrySet().iterator();
        Iterator i2 = map2.entrySet().iterator();

        while (i1.hasNext())
        {
            Map.Entry entry1 = (Map.Entry) i1.next();
            Map.Entry entry2 = (Map.Entry) i2.next();

            // Must split the Key and Value so that Map.Entry's equals() method is not used.
            DualKey dk = new DualKey(entry1.getKey(), entry2.getKey(), stackPosition.add("key(" + entry1.getKey() + "!=" + entry2.getKey() + ")"));
            if (!visited.contains(dk))
            {   // Push Keys for further comparison
                stack.addFirst(dk);
            }

            dk = new DualKey(entry1.getValue(), entry2.getValue(), stackPosition.add("get(" + entry1.getKey() + ")"));
            if (!visited.contains(dk))
            {   // Push values for further comparison
                stack.addFirst(dk);
            }
        }
    }

    /**
     * Deeply compare two Map instances.  After quick short-circuit tests, this method
     * uses a temporary Map so that this method can run in O(N) time.
     *
     * @param map1          Map one
     * @param map2          Map two
     * @param stack         add items to compare to the Stack (Stack versus recursion)
     * @param visited       Set containing items that have already been compared, to prevent cycles.
     * @param namesToIgnore
     * @param stackPosition
     * @param failures
     * @param inverseIgnore
     */
    @SuppressWarnings("unchecked")
    private static void compareUnorderedMap(Map map1, Map map2, LinkedList stack, Set visited, List<String> namesToIgnore, DataStackTrace stackPosition, Set<Failure> failures, Boolean inverseIgnore)
    {
        // Same instance check already performed...

        if (map1.size() != map2.size())
        {
            failWithStack(failures, "Maps sizes are not equal", stackPosition);
            return;
        }

        Map fastLookup = new HashMap();

        for (Map.Entry entry : (Set<Map.Entry>) map2.entrySet())
        {
            fastLookup.put(deepHashCode(entry.getKey(), namesToIgnore, inverseIgnore), entry);
        }

        for (Map.Entry entry : (Set<Map.Entry>) map1.entrySet())
        {
            Map.Entry other = (Map.Entry) fastLookup.get(deepHashCode(entry.getKey(), namesToIgnore, inverseIgnore));
            if (other == null)
            {
                failWithStack(failures, "Entry of map is null", stackPosition.add("get(" + entry.getKey() + ")"));
                return;
            }

            DualKey dk = new DualKey(entry.getKey(), other.getKey(), stackPosition.add("get(" + entry.getKey() + ")"));
            if (!visited.contains(dk))
            {   // Push keys for further comparison
                stack.addFirst(dk);
            }

            dk = new DualKey(entry.getValue(), other.getValue(), stackPosition.add("get(" + entry.getKey() + ")"));
            if (!visited.contains(dk))
            {   // Push values for further comparison
                stack.addFirst(dk);
            }
        }

    }

    private static void failWithStack(Set<Failure> failures, String message, DataStackTrace stack)
    {
        failures.add(new Failure(message, stack));
    }

    /**
     * Determine if the passed in class has a non-Object.equals() method.  This
     * method caches its results in static ConcurrentHashMap to benefit
     * execution performance.
     *
     * @param c Class to check.
     * @return true, if the passed in Class has a .equals() method somewhere between
     *         itself and just below Object in it's inheritance.
     */
    public static boolean hasCustomEquals(Class c)
    {
        Class origClass = c;
        if (_customEquals.containsKey(c))
        {
            return _customEquals.get(c);
        }

        while (!Object.class.equals(c))
        {
            try
            {
                c.getDeclaredMethod("equals", Object.class);
                _customEquals.put(origClass, true);
                return true;
            } catch (Exception ignored)
            {
            }
            c = c.getSuperclass();
        }
        _customEquals.put(origClass, false);
        return false;
    }

    /**
     * Get a deterministic hashCode (int) value for an Object, regardless of
     * when it was created or where it was loaded into memory.  The problem
     * with java.lang.Object.hashCode() is that it essentially relies on
     * memory location of an object (what identity it was assigned), whereas
     * this method will produce the same hashCode for any object graph, regardless
     * of how many times it is created.<br/><br/>
     * <p/>
     * This method will handle cycles correctly (A->B->C->A).  In this case,
     * Starting with object A, B, or C would yield the same hashCode.  If an
     * object encountered (root, suboject, etc.) has a hashCode() method on it
     * (that is not Object.hashCode()), that hashCode() method will be called
     * and it will stop traversal on that branch.
     *
     * @param obj           Object who hashCode is desired.
     * @param namesToIgnore
     * @param inverseIgnore
     * @return the 'deep' hashCode value for the passed in object.
     */
    @SuppressWarnings("unchecked")
    public static int deepHashCode(Object obj, List<String> namesToIgnore, Boolean inverseIgnore)
    {
        Set visited = new HashSet();
        LinkedList<Object> stack = new LinkedList<Object>();
        stack.addFirst(obj);
        int hash = 0;

        while (!stack.isEmpty())
        {
            obj = stack.removeFirst();
            if (obj == null || visited.contains(obj))
            {
                continue;
            }

            visited.add(obj);

            if (obj.getClass().isArray())
            {
                int len = Array.getLength(obj);
                for (int i = 0; i < len; i++)
                {
                    stack.addFirst(Array.get(obj, i));
                }
                continue;
            }

            if (obj instanceof Collection)
            {
                stack.addAll(0, (Collection) obj);
                continue;
            }

            if (obj instanceof Map)
            {
                stack.addAll(0, ((Map) obj).keySet());
                stack.addAll(0, ((Map) obj).values());
                continue;
            }

            if (hasCustomHashCode(obj.getClass()))
            {   // A real hashCode() method exists, call it.
                hash += obj.hashCode();
                continue;
            }

            Collection<Field> fields = getDeepDeclaredFields(obj.getClass(), namesToIgnore, inverseIgnore);
            for (Field field : fields)
            {
                try
                {
                    stack.addFirst(field.get(obj));
                } catch (Exception ignored)
                {
                }
            }
        }
        return hash;
    }

    /**
     * Determine if the passed in class has a non-Object.hashCode() method.  This
     * method caches its results in static ConcurrentHashMap to benefit
     * execution performance.
     *
     * @param c Class to check.
     * @return true, if the passed in Class has a .hashCode() method somewhere between
     *         itself and just below Object in it's inheritance.
     */
    public static boolean hasCustomHashCode(Class c)
    {
        Class origClass = c;
        if (_customHash.containsKey(c))
        {
            return _customHash.get(c);
        }

        while (!Object.class.equals(c))
        {
            try
            {
                c.getDeclaredMethod("hashCode");
                _customHash.put(origClass, true);
                return true;
            } catch (Exception ignored)
            {
            }
            c = c.getSuperclass();
        }
        _customHash.put(origClass, false);
        return false;
    }

    /**
     * Get all non static, non transient, fields of the passed in class.
     * The special this$ field is also not returned.  The result is cached
     * in a static ConcurrentHashMap to benefit execution performance.
     *
     * @param c             Class instance
     * @param namesToIgnore
     * @param inverseIgnore
     * @return Collection of only the fields in the passed in class
     *         that would need further processing (reference fields).  This
     *         makes field traversal on a class faster as it does not need to
     *         continually process known fields like primitives.
     */
    public static Collection<Field> getDeepDeclaredFields(Class c, List<String> namesToIgnore, Boolean inverseIgnore)
    {
        if (_reflectedFields.containsKey(c))
        {
            return _reflectedFields.get(c);
        }
        Collection<Field> fields = new ArrayList<Field>();
        Class curr = c;

        while (curr != null)
        {
            try
            {
                Field[] local = curr.getDeclaredFields();

                for (Field field : local)
                {
                    if (!field.isAccessible())
                    {
                        try
                        {
                            field.setAccessible(true);
                        } catch (Exception ignored)
                        {
                        }
                    }

                    int modifiers = field.getModifiers();
                    if (!Modifier.isStatic(modifiers) && !field.getName().startsWith("this$") && (inverseIgnore ^ !namesToIgnore.contains(field.getName())) && !Modifier.isTransient(modifiers))
                    {   // speed up: do not count static fields, not go back up to enclosing object in nested case
                        fields.add(field);
                    }
                }
            } catch (ThreadDeath t)
            {
                throw t;
            } catch (Throwable ignored)
            {
            }

            curr = curr.getSuperclass();
        }
        _reflectedFields.put(c, fields);
        return fields;
    }

}
