package com.deals.tests.messages;

import com.deals.model.messages.message_types.DiscussionMessage;
import com.deals.model.messages.message_types.Message;
import com.deals.model.product.GroupDeal;
import com.deals.model.product.ProductInfo;
import com.deals.model.users.User;
import com.deals.model.users.roles.Administrator;
import com.deals.model.users.roles.Organizer;
import com.deals.model.users.roles.Role;
import com.deals.services.ProductService;
import com.deals.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: artem
 * Date: 1/6/12
 * Time: 8:28 PM
 * To change this template use File | Settings | File Templates.
 */
@ContextConfiguration("classpath*:conf/spring/test-spring-context.xml")
@Slf4j
public class MessagesServiceTest extends AbstractTestNGSpringContextTests
{
    @Autowired UserService userService;
    @Autowired SessionFactory sessionFactory;
    @Autowired ProductService productService;

    @Test
    public void test()
    {
        Session session = SessionFactoryUtils.getSession(sessionFactory, true);
        TransactionSynchronizationManager.bindResource(sessionFactory, new SessionHolder(session));

        // Create User with Roles : Administrator and Organizer
        User user = new User();
        user.getUserInfo().setEmail("a@a.com");
        user.getUserInfo().setName("artem");
        user.getUserInfo().setPassword("password");
        user.setIsBanned(false);

        Administrator admin = new Administrator();
        user.getRoles().add(admin);

        Organizer organizer = new Organizer();
        user.getRoles().add(organizer);

        userService.save(user);

        log.info("User saved");

        // Create group deal
        GroupDeal groupDeal = new GroupDeal();
        groupDeal.setOrganizer(organizer);
        groupDeal.setState(GroupDeal.GroupDealState.REQUESTS_COLLECTION);

        groupDeal.addNewInfoVersion(new ProductInfo());

        productService.save(groupDeal);

        log.info("Product saved");

        // Test messaging system
        List<GroupDeal> groupDeals = productService.loadAll(GroupDeal.class);
        GroupDeal groupDealB = groupDeals.get(0);

        List<User> users = userService.loadAll(User.class);
        Role role = users.get(0).getRoles().get(0);
        DiscussionMessage message = new DiscussionMessage();
        message.setSender(role);
        message.setText("Hello world!");
        message.setTime(new Date());
        role.getMessageBox().send(groupDealB.getMessageBox(), message);

        log.info("Message sent");

        List<Message> allDiscussionMessages = groupDealB.getMessageBox().getAllDiscussionMessages();
        Assert.assertEquals(allDiscussionMessages.size(), 1);
        log.info(Arrays.toString(allDiscussionMessages.toArray()));

        SessionHolder sessionHolder = (SessionHolder) TransactionSynchronizationManager.unbindResource(sessionFactory);
        SessionFactoryUtils.closeSession(sessionHolder.getSession());
    }
}
