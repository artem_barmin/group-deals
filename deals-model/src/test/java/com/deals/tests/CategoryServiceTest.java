package com.deals.tests;

import com.deals.model.product.Category;
import com.deals.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.sort;
import static ch.lambdaj.collection.LambdaCollections.with;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

/**
 * Tests for category service
 */
@ContextConfiguration("classpath*:conf/spring/test-spring-context.xml")
public class CategoryServiceTest extends AbstractTestNGSpringContextTests
{
    @Autowired
    CategoryService categoryService;

    @Autowired
    TransactionTemplate transactionTemplate;

    private List<String> categoriesToStrings(List<Category> categories)
    {
        return with(sort(categories, on(Category.class).getName())).extract(on(Category.class).getName());
    }

    @BeforeClass
    public void init()
    {
        transactionTemplate.execute(new TransactionCallback<Object>()
        {
            public Object doInTransaction(TransactionStatus status)
            {
//                             ___________category1__________
//                            |                             |
//                  ______category2_____                category3
//                 |                    |                   |
//          ___category4______      category5           category8
//         |                  |
//     category6          category7

                Category category1 = new Category("1", "1", null);
                Category category2 = new Category("2", "2", category1);
                Category category3 = new Category("3", "3", category1);

                Category category4 = new Category("4", "4", category2);
                Category category5 = new Category("5", "5", category2);

                Category category6 = new Category("6", "6", category4);
                Category category7 = new Category("7", "7", category4);

                Category category8 = new Category("8", "8", category3);

                categoryService.save(category1);
                categoryService.save(category2);
                categoryService.save(category3);
                categoryService.save(category4);
                categoryService.save(category5);
                categoryService.save(category6);
                categoryService.save(category7);
                categoryService.save(category8);

                return null;
            }
        });
    }

//    The same as above, just ot keep near the eyes while writing the tests:
//                             ___________category1__________
//                            |                             |
//                  ______category2_____                category3
//                 |                    |                   |
//          ___category4______      category5           category8
//         |                  |
//     category6          category7

    @Test
    public void testEndPoints()
    {
        List<Category> allEndCategories = categoryService.getAllEndCategories();
        Assert.assertEquals(categoriesToStrings(allEndCategories), asList("5", "6", "7", "8"));
    }

    @Test
    public void testTopLevel()
    {
        List<Category> allTopLevelCategories = categoryService.getAllTopLevelCategories();
        Assert.assertEquals(categoriesToStrings(allTopLevelCategories), asList("1"));
    }

    @DataProvider(name = "pathResolving")
    public Object[][] pathResovlingDataProvider()
    {
        return new Object[][] {
                {"1/2/4/7", asList("1","2","4", "7")},
                {"1/3/8", asList("1","3","8")},
                {"1/3/5/6", emptyList()},
                {"bad_bad_category", emptyList()},
                {"1/2/4/6/bad_bad_category", emptyList()},
                {"8/3/1", emptyList()}
        };
    }

    @Test(dataProvider = "pathResolving")
    public void testPathResolving(String path, List<String> expectedResult)
    {
        List<Category> categoryPathByStringPath = categoryService.getCategoryPathByStringPath(path);
        Assert.assertEquals(categoriesToStrings(categoryPathByStringPath), expectedResult);
    }

    @DataProvider(name = "partialEndCategories")
    public Object[][] partialEndCategoriesDataProvider()
    {
        return new Object[][] {
                {"1/2/5",asList("5")},
                {"1/2", asList("5", "6", "7")},
                {"1", asList("5", "6", "7", "8")},
                {"1/2/4", asList("6", "7")},
                {"1/3", asList("8")},
                {"bad_category", emptyList()},
                {"1/2/bad_category", emptyList()}
        };
    }

    @Test(dataProvider = "partialEndCategories")
    public void testPartialEndCategoriesFetching(String path, List<String> expectedResult)
    {
        List<Category> allEndCategoriesWithGivenParent = categoryService.getAllEndCategoriesWithGivenParent(categoryService.getCurrentCategory(path));
        Assert.assertEquals(categoriesToStrings(allEndCategoriesWithGivenParent), expectedResult);
    }

    @DataProvider(name = "currentCategoryFromPath")
    public Object[][] currentCategoryFromPathDataprovider()
    {
        return new Object[][]{
                {"1/2/4", "4"},
                {"1/2", "2"},
                {"1/2/4/6", "6"},
                {"1/3", "3"},
                {"bad_category", null},
                {"1/2/4/bad_category", null},
        };
    }

    @Test(dataProvider = "currentCategoryFromPath")
    public void testCurrentCategoryRetrieving(String path, String expectedName)
    {
        Category currentCategory = categoryService.getCurrentCategory(path);
        if (currentCategory == null)
            Assert.assertEquals(currentCategory, expectedName);
        else
            Assert.assertEquals(currentCategory.getShortName(), expectedName);
    }
}
