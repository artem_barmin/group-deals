package com.deals.tests;

import com.deals.model.users.User;
import com.deals.model.users.roles.Administrator;
import com.deals.model.users.roles.Role;
import com.deals.services.UserService;
import com.deals.tests.support.DeepEquals;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ContextConfiguration("classpath*:conf/spring/test-spring-context.xml")
public class UserServiceTest extends AbstractTestNGSpringContextTests
{

    @Autowired UserService userService;
    @Autowired SessionFactory sessionFactory;

    @Test
    public void test()
    {
        Session session = SessionFactoryUtils.getSession(sessionFactory, true);
        TransactionSynchronizationManager.bindResource(sessionFactory, new SessionHolder(session));

        User user = new User();
        user.setIsBanned(false);
        user.getRoles().add(new Administrator());

        user.getUserInfo().setEmail("artem@a.com");
        user.getUserInfo().setName("hello");
        user.getUserInfo().setPassword("password");

        userService.save(user);

        List<User> users = userService.loadAll(User.class);
        Assert.assertEquals(users.size(), 1);
        DeepEquals.assertEquals(users.get(0), user, Arrays.asList("id"));

        SessionHolder sessionHolder = (SessionHolder) TransactionSynchronizationManager.unbindResource(sessionFactory);
        SessionFactoryUtils.closeSession(sessionHolder.getSession());
    }
}
