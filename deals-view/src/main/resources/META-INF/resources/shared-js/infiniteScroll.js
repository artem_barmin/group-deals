(function($)
{
    $.INFINITE_SCROLL_DEBUG = false;

    $.fn.infiniteScroll = function(options)
    {
        function debug()
        {
            if ($.INFINITE_SCROLL_DEBUG)
                console.log.apply(this, arguments);
        }

        var element = $(this);

        var requestInProcess = false;

        var defaults = {
            bottomOffset : 500,
            limit: 10
        };

        var opts = $.extend(defaults, options);

        debug("Init with options : ", opts);

        var offset = 0;

        $(window).scroll(function()
        {
            getCurrentPosition();
            if (($(window).scrollTop() + $(window).height()) > ($(document).height() - opts.bottomOffset))
            {
                updateDataFromJsf();
            }
        });

        addElements();

        offset = opts.limit;

        function addElements()
        {
            var items = element.find(".infinite-data-source").find(".infinite-scroll-item");
            if (items.size() > 0)
            {
                var container = element.find(".infinite-visible");
                container.append($("<div style='display: none' class='infinite-offset'>" + offset + "</div>"));
                container.append(items);
                offset += opts.limit;
                debug("Add elements", items);
            }
        }

        function getCurrentPosition()
        {
            var currentPosition = $(window).scrollTop() + $('.infinite-visible').offset().top;
            var selectedElement = null;
            var minimalOffset = -Infinity;
            $('.infinite-offset').each(function(i, elt)
            {
                // take offset - next visible element, and compute it's offset
                var nearestElement = $(elt).next();
                if (nearestElement.length == 0)
                    nearestElement = $(elt).prev();
                var currentOffset = nearestElement.offset().top - currentPosition;
                debug("Nearest element to : ", elt, " is ", nearestElement, " with offset ", currentOffset);
                if (currentOffset < 0)
                {
                    if (currentOffset > minimalOffset)
                    {
                        currentOffset = minimalOffset;
                        selectedElement = elt;
                    }
                }
            });
            debug("Choosed element for page computation : ", selectedElement);
            $('[id$="window"]').find('.page-number').text($(selectedElement).text());
        }

        function updateDataFromJsf()
        {
            debug("Request for more data. Offset : ", offset, "Limit : ", opts.limit, " render id:", element.find(".infinite-data-source").attr("id"));
            if (!requestInProcess)
            {
                requestInProcess = true;
                jsf.ajax.request(
                        this,
                        null,
                        {
                            firstRequest: true,
                            clientSideRequest: true,
                            offset: offset,
                            limit: opts.limit,
                            render: element.find(".infinite-data-source").attr("id"),
                            onevent: handleEvent,
                            onerror: handleError
                        });
            }
        }

        function handleEvent(event)
        {
            if (event.status == 'success')
            {
                addElements();
                requestInProcess = false;
            }
        }

        function handleError(event)
        {

        }

    };
})(jQuery);