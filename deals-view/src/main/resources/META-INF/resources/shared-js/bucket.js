var initialBucketOffset;

$(document).ready(function()
{
    initialBucketOffset = $("#bucket").offset().top;
    createSticker(1);
    createSticker(2);
    createSticker(3);
});

function createSticker(i)
{
    var sticker = $("<div id='sticker" + i + "'/>");
    sticker.css('position', 'relative');
    sticker.css('background-image', 'url(/resources/images/sticker.png)');
    sticker.css('background-repeat', 'no-repeat');
    sticker.css('left', '250px');
    sticker.css('top', (i * 50) + "px");
    sticker.css('z-index', '2');

    $('#sticker-container').append(sticker);
    var r = Raphael("sticker" + i, 120, 70);
    var text = r.text(55, 32, "450 грн", 40);
    text.rotate(22, 0);
    text.attr({ "font-size": 16, "font-family": "Arial, Helvetica, sans-serif", "font-weight": 'bold'});
    text.attr("fill", "#ffffff");

}

$(window).scroll(function()
{
    var bucket = $("#bucket");

    if ($(window).scrollTop() < initialBucketOffset)
    {
        bucket.css("position", "static");
    }
    else if (($(window).scrollTop() + 20) > bucket.offset().top)
    {
        var oldLeft = bucket.offset().left;
        var fixedTop = bucket.offset().top - $(window).scrollTop();

        if (8 > fixedTop)
            fixedTop = 8;

        bucket.css("position", "fixed");
        bucket.css("top", fixedTop + "px");
        bucket.css("left", oldLeft + "px");
    }
});