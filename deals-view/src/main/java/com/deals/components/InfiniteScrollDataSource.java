package com.deals.components;

import java.util.List;

/**
 * Interface that should be implemented by infinite datascroll sources. In most
 * of cases you should not implement it directly, but must use delegation
 * mechanism provided by lombok.
 */
public interface InfiniteScrollDataSource<T>
{
    List<T> infiniteValues(Integer initialLimit);
}
