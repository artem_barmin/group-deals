package com.deals.components;

import com.deals.support.JsfUtils;
import lombok.extern.slf4j.Slf4j;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

/**
 * Base class for all datasources of infinite scroll components. It is responsible for calling low level datasource(like DB),
 * process parameters from client side of component and caching.
 */
@Slf4j
public abstract class AbstractInfiniteScrollDataSource<T>
{
    protected abstract List<T> values(Integer limit, Integer offset);

    public List<T> infiniteValues(Integer initialLimit)
    {
        Map<String, String> requestParameterMap = JsfUtils.getRequestParameterMap();

        if (requestParameterMap.containsKey("clientSideRequest"))
        {
            Integer limit = Integer.valueOf(requestParameterMap.get("limit"));
            Integer offset = Integer.valueOf(requestParameterMap.get("offset"));

            List<T> values = values(limit, offset);

            log.debug(MessageFormat.format("Limit : {0}, offset : {1}, result size : {2}",limit,offset,values.size()));

            return values;
        } else
        {
            List<T> values = values(initialLimit, 0);

            log.debug(MessageFormat.format("Limit : {0}, result size : {1}", initialLimit, values.size()));

            return values;
        }
    }

}
