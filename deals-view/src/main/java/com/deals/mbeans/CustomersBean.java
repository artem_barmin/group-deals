package com.deals.mbeans;

import com.deals.components.AbstractInfiniteScrollDataSource;
import com.deals.model.users.User;
import com.deals.model.users.UserInfo;
import com.deals.model.users.roles.Administrator;
import com.deals.model.users.roles.Role;
import com.deals.services.UserService;
import com.deals.support.ConversationBean;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(value = "customersBean")
@Scope("conversation.access")
@Slf4j
public class CustomersBean extends ConversationBean
{

    @Autowired
    private UserService userService;

    @Getter
    private User user;

    @Getter
    private CustomersInfiniteDatasource customersInfiniteDatasource = new CustomersInfiniteDatasource();

    public CustomersBean()
    {
        user = new User();
        user.setUserInfo(new UserInfo());
        user.setRoles(new ArrayList<Role>());
        user.getRoles().add(new Administrator());
        user.getRoles().add(new Administrator());
    }

    public void save()
    {
        log.info("User {} is saving", user);
        userService.save(user);
    }

    public List<User> getAllCustomers()
    {
        return userService.loadAll(User.class);
    }

    public class CustomersInfiniteDatasource extends AbstractInfiniteScrollDataSource<User>
    {

        @Override
        protected List<User> values(Integer limit, Integer offset)
        {
            return userService.loadAll(User.class, limit, offset);
        }
    }
}
