package com.deals.mbeans;

import com.deals.components.AbstractInfiniteScrollDataSource;
import com.deals.components.InfiniteScrollDataSource;
import com.deals.model.product.GroupDeal;
import com.deals.model.product.Product;
import com.deals.model.product.Stock;
import com.deals.services.ProductService;
import lombok.Delegate;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(value = "singleGDManagementBean")
@Scope("conversation.access")
@Slf4j
public class SingleGDManagementBean implements InfiniteScrollDataSource  {

    @Autowired
    private ProductService productService;

    @Getter @Setter
    private Long id;

    @Getter @Setter
    private GroupDeal groupDeal;

    @Delegate(types = InfiniteScrollDataSource.class)
    private StockInfiniteDataSource stockInfiniteDataSource = new StockInfiniteDataSource();

    public class StockInfiniteDataSource extends AbstractInfiniteScrollDataSource<Stock> {

        @Override
        protected List<Stock> values(Integer limit, Integer offset) {
            return productService.loadAll(Stock.class, limit, offset);
        }
    }

    public void init()
    {
        groupDeal = (GroupDeal) productService.get(Product.class, id);
    }
}
