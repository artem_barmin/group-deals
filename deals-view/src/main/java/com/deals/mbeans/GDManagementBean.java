package com.deals.mbeans;

import ch.lambdaj.function.convert.PropertyExtractor;
import com.deals.components.AbstractInfiniteScrollDataSource;
import com.deals.components.InfiniteScrollDataSource;
import com.deals.model.product.Category;
import com.deals.model.product.Product;
import com.deals.services.CategoryService;
import com.deals.services.ProductService;
import lombok.Data;
import lombok.Delegate;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import java.util.ArrayList;
import java.util.List;

import static ch.lambdaj.Lambda.convert;

/**
 * Bean for list of group deals.
 * <p/>
 * Responsibility:
 * 1. Load list of all group deals with specified filter
 * 2. Create path from current page, up - for breadcrumbs
 * 3. Create list of currently available categories(according to current level)
 */
@Component(value = "gdManagementBean")
@Scope("conversation.access")
@Slf4j
public class GDManagementBean implements InfiniteScrollDataSource
{

    @Setter @Getter
    private String categoryPath;

    @Autowired
    private ProductService groupDealsService;

    @Autowired
    private CategoryService categoryService;

    @Delegate(types = InfiniteScrollDataSource.class)
    private GDInfiniteScrollDataSource gdInfiniteDataSource = new GDInfiniteScrollDataSource();

    public void categoryValidator(FacesContext context, UIComponent component, Object value)
    {
        String strValue = (String) value;
        if (strValue.length() < 200)
        {
            if (strValue.matches("([a-zA-Z]+/?)*"))
                return;
        }
        throw new ValidatorException(new FacesMessage("Указанной категории не существует"));
    }

    public List<BreadcrumbItem> getBreadCrumbs()
    {
        List<Category> categoryPathByStringPath = categoryService.getCategoryPathByStringPath(categoryPath);
        List<BreadcrumbItem> result = new ArrayList<BreadcrumbItem>();

        String previousPath = "";
        for (Category category : categoryPathByStringPath)
        {
            result.add(new BreadcrumbItem(category.getName(), previousPath + category.getShortName()));
            previousPath += category.getShortName() + "/";
        }

        return result;
    }

    public List<BreadcrumbItem> getCategories()
    {
        List<Category> categoryPathByStringPath = categoryService.getAvailableCategories(categoryPath);
        return convert(categoryPathByStringPath, new CategoryBreadCrumbConverter(null));
    }

    @Data
    public class BreadcrumbItem
    {
        private String value;
        private String link;

        public BreadcrumbItem(String value, String link)
        {
            this.value = value;
            this.link = link;
        }
    }

    public class CategoryBreadCrumbConverter extends PropertyExtractor<Category, BreadcrumbItem>
    {

        public CategoryBreadCrumbConverter(String propertyName)
        {
            super(propertyName);
        }

        @Override
        public BreadcrumbItem convert(Category from)
        {
            return new BreadcrumbItem(from.getName(), from.getShortName());
        }

    }

    public class GDInfiniteScrollDataSource extends AbstractInfiniteScrollDataSource<Product>
    {
        @Override
        protected List<Product> values(Integer limit, Integer offset)
        {
            Category currentCategory = categoryService.getCurrentCategory(categoryPath);
            return groupDealsService.getProductsInCategory(currentCategory, limit, offset);
        }
    }

}
