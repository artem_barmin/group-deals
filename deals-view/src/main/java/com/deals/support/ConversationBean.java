package com.deals.support;

import org.apache.myfaces.orchestra.conversation.ConversationBindingEvent;
import org.apache.myfaces.orchestra.conversation.ConversationBindingListener;

/**
 * This is parent of all conversation scoped beans(it is similar to view scope).
 *
 * It have following methods:
 * 1. init - invoked when conversation starts(must be used instead of constructor and @PostConstruct)
 * 2. destroy - invoked when conversation is ended
 *
 * If you want to have custom logic bound on this events - you must override them. No super.init() should be called,
 * because, by default, they are empty.
 */
public class ConversationBean implements ConversationBindingListener
{
    public void valueBound(ConversationBindingEvent event)
    {
        init();
    }

    protected void init()
    {
    }

    public void valueUnbound(ConversationBindingEvent event)
    {
        destroy();
    }

    protected void destroy()
    {
    }
}
