package com.deals.support;

import javax.faces.context.FacesContext;
import java.util.Map;

/**
 * Misc utility function for work with JSF
 */
public class JsfUtils
{
    public static Map<String, String> getRequestParameterMap()
    {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
    }

    public static Map<String, Object> getRequestMap()
    {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
    }
}
